import core from "@actions/core";
import * as hc from '@actions/http-client';
import fs from 'fs'
import FormData from 'form-data'

async function run() {
    try {
        const token = core.getInput("token", { required: true })
        const appVersion = core.getInput("app_version", { required: true })
        const appID = core.getInput("app_id", { required: true })
        const appFile = core.getInput("app_file", { required: true })
        const description = core.getInput("description")
        const sandbox = core.getInput("sandbox", {}) || false

        const client = new hc.HttpClient('ocamba-publish', [], {
            allowRetries: true,
            maxRetries: 3
        });

        const form = new FormData()
        form.append('file', fs.createReadStream(appFile))
        form.append('description', 'development')
        let headers = form.getHeaders()
        headers.Authorization = `Bearer ${token}`

        let url = sandbox ? 'https://dev-api.ocamba.com' : 'https://api.ocamba.com'

        let response = await client.put(`${url}/v2/ocamba/developer/apps/${appID}/versions/${appVersion}`, form, headers)

        const body = await response.readBody();
        const statusCode = response.message.statusCode || 500;
        if (statusCode >= 400) {
            throw new Error(`(${statusCode}) ${body}`)
        }

        let data = JSON.parse(body)
        core.setOutput('artifact_url', data.source)
        console.log(`🎉 App ready at ${data.source}`)
    } catch (error) {
        if (core.isDebug()) console.error(error)
        core.setFailed(error.message);
    }
}

run()