import resolve from '@rollup/plugin-node-resolve'
import commonjs from '@rollup/plugin-commonjs'
import terser from '@rollup/plugin-terser'
import strip from '@rollup/plugin-strip'
import json from "@rollup/plugin-json"

const isProduction = process.env.NODE_ENV === 'prod'

export default {
    input: 'index.js',
    output: {
        dir: 'dist',
        format: 'module'
    },
    plugins: [
        json(),
        resolve(),
        commonjs(),
        isProduction && strip(),
        isProduction && terser()
    ]
}
